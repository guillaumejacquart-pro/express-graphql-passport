const express = require("express");
const HttpStatus = require("http-status-codes");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");
const crypto = require("crypto");
const promisify = require("util").promisify;

const config = require("../config");
const smtpTransport = require("../services/mail");
const User = require("../model/user");
const CONSTANTS = require("../constants");

/* POST login. */
router.post("/login", function(req, res, next) {
  passport.authenticate("local", { session: false }, (err, user, info) => {
    if (err || !user) {
      return res.status(400).json({
        message: "Something is not right",
        user: user
      });
    }
    req.login(user, { session: false }, err => {
      if (err) {
        res.send(err);
      }

      const userData = {
        id: user.id,
        email: user.email
      };

      // generate a signed son web token with the contents of user object and return it in the response
      const token = jwt.sign(userData, CONSTANTS.JWT_SECRET);
      return res.json({ user: userData, token });
    });
  })(req, res);
});

/* POST register. */
router.post("/register", async function(req, res, next) {
  try {
    const userDb = await User.create(req.body);

    return res.json({
      id: userDb.id,
      email: userDb.email
    });
  } catch (e) {
    return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
  }
});

/* POST reset password. */
router.post("/forgot-password", async function(req, res, next) {
  try {
    const userDb = await User.findOneByEmail(req.body.email);
    if (!userDb) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Email does not exists" });
    }
    const buffer = await promisify(crypto.randomBytes)(20);
    const token = buffer.toString("hex");

    await User.updateOne(userDb.id, {
      reset_token: token,
      reset_token_expires: Date.now() + 86400000
    });

    const port = config.get("port");
    const fullUrl =
      req.protocol +
      "://" +
      req.hostname +
      (port == 80 || port == 443 ? "" : ":" + port);

    var data = {
      to: userDb.email,
      from: config.get('mail.from'),
      template: "forgot-password-email",
      subject: "Password help has arrived!",
      context: {
        url: fullUrl + "/auth/reset-password?token=" + token
      }
    };

    smtpTransport.sendMail(data, function(err) {
      if (!err) {
        return res.json({
          message: "Kindly check your email for further instructions"
        });
      } else {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      }
    });
  } catch (e) {
    return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
  }
});

/* GET reset password */
router.get("/reset-password", async function(req, res, next) {
  if(!req.query.token){
    return next(new Error("Missing token"))
  }

  return res.render('reset-password', {
    token: req.query.token
  })
})

/* POST reset password. */
router.post("/reset-password", async function(req, res, next) {
  try {
    if(req.body.password !== req.body.confirmPassword){
      return res.render('reset-password', {
        token: req.query.token,
        error: 'Passwords do not match'
      })
    }
    const userDb = await User.findOneByResetToken(
      req.query.token
    );
    if (!userDb) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Reset token invalid or expired" });
    }

    await User.updatePassword(userDb.id, req.body.password);

    var data = {
      to: userDb.email,
      from: config.get('mail.from'),
      template: "reset-password-email",
      subject: "Password Reset Confirmation"
    };

    smtpTransport.sendMail(data, function(err) {
      if (!err) {
        return res.render('reset-password-confirmation')
      } else {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
      }
    });
  } catch (e) {
    return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
  }
});

module.exports = router;
