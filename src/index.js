import express from "express"
import bodyParser from "body-parser"
import passport from "passport"
import { graphqlExpress, graphiqlExpress } from "apollo-server-express"
import cors from "cors"

// Firebase configuration
import auth from "./authentication"

// Config
import config from "./config.js"

// GraphQL Schema
import schema from "./graphql/schema"

// Initialize the app
const app = express();
app.use(cors());

// Template engine
app.set("views", __dirname + "/views");
app.set("view engine", "pug");

// Body parser
app.use(bodyParser.json());

// Static files
app.use("/static", express.static(__dirname + "/static"));

// GraphQL with authentication
app.use(
  "/graphql",
  auth,
  graphqlExpress(req => {
    return {
      schema,
      context: {
        user: req.user
      }
    };
  })
);

// GraphiQL, a visual editor for queries
app.use("/graphiql", graphiqlExpress({ endpointURL: "/graphql" }));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    stack: err.stack
  });
});

// Start the server
app.listen(config.get("port"), () => {
  console.log(`Go to http://localhost:${config.get("port")}/graphiql to run queries!`);
});
