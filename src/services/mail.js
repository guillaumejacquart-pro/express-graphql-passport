const hbs = require("nodemailer-express-handlebars"),
  nodemailer = require("nodemailer"),
  path = require("path"),
  config = require("../config");

var smtpTransport = nodemailer.createTransport({
  host: config.get("mail.host"),
  port: config.get("mail.port"),
  auth: {
    user: config.get("mail.username"),
    pass: config.get("mail.password")
  }
});

var handlebarsOptions = {
  viewEngine: "handlebars",
  viewPath: path.join(__dirname, "../templates/"),
  extName: ".html"
};

smtpTransport.use("compile", hbs(handlebarsOptions));

module.exports = smtpTransport;
