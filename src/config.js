const convict = require("convict");
const fs = require("fs");

// Define a schema
const config = convict({
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV"
  },
  port: {
    doc: "The port to bind.",
    format: "port",
    default: 3000,
    env: "PORT",
    arg: "port"
  },
  firebaseUrl: {
    doc: "The firebase URL",
    format: "*",
    default: "https://daveapp-8db4a.firebaseio.com",
    env: "FIREBASE_URL"
  },
  db: {
    dialect: {
      doc: "Database dialect",
      format: "*",
      default: "sqlite",
      env: "DB_DIALECT"
    },
    host: {
      doc: "Database host name/IP",
      format: "*",
      default: "server1.dev.test",
      env: "DB_HOST"
    },
    name: {
      doc: "Database name",
      format: String,
      default: "users",
      env: "DB_NAME"
    },
    username: {
      doc: "Database username",
      format: String,
      default: "root"
    },
    password: {
      doc: "Database password",
      format: String,
      default: "",
      env: "DB_PASSWORD"
    },
    storage: {
      doc: "Database storage path (sqlite)",
      format: String,
      default: "db.sqlite",
      env: "DB_STORAGE"
    }
  },
  mail: {
    host: {
      doc: "SMTP host name/IP",
      format: "*",
      default: "in-v3.mailjet.com",
      env: "SMTP_HOST"
    },
    port: {
      doc: "SMTP name",
      format: Number,
      default: 587,
      env: "SMTP_PORT"
    },
    username: {
      doc: "SMTP username",
      format: String,
      default: "eab6b2cdb614263f0c3dcbfa91908af2",
      env: "SMTP_USERNAME"
    },
    password: {
      doc: "SMTP password",
      format: String,
      default: "ea271d9cb488b25061d1f2af0f54192f",
      env: "SMTP_PASSWORD"
    },
    from: {
      doc: "SMTP from",
      format: String,
      default: "dev@guillaumejacquart.com",
      env: "SMTP_FROM"
    }
  }
});

// Load environment dependent configuration
const env = config.get("env");

const envConfig = `./config.${env}.json`;
if (fs.existsSync(envConfig)) {
  config.loadFile(envConfig);
}

// Perform validation
config.validate({
  allowed: "strict"
});

module.exports = config;
