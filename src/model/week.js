import Sequelize from "sequelize";

const sequelize = require("./connection.js");

const Food = require("./food");
const FoodWeek = require("./foodWeek");

const Week = sequelize.define("week", {
  number: { type: Sequelize.SMALLINT }
});

Week.belongsToMany(Food, { through: FoodWeek.Model })
Food.belongsToMany(Week, { through: FoodWeek.Model })

async function create(week) {
  const weekCreated = await Week.create(week);

  return weekCreated;
}

async function getAll() {
  return Week.findAll();
}

sequelize.sync({
  alter: false
});

module.exports = {
  create,
  getAll
};
