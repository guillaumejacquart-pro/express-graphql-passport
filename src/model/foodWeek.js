import Sequelize from "sequelize";

const sequelize = require("./connection.js");

const FoodWeek = sequelize.define("foodWeek", {
  quantity: { type: Sequelize.STRING }
});

sequelize.sync({
  alter: false
});

async function create(foodWeek) {
  return await FoodWeek.create(foodWeek);
}

async function getWeekFoods(weekId) {
  FoodWeek.findAll({
    where: {
      weekId
    },
    include: ["Food"]
  });
}

module.exports = {
  create,
  getWeekFoods,
  Model: FoodWeek
};
