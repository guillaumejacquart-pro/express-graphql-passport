const Sequelize = require("sequelize");
const sequelize = require("./connection.js");

const Food = sequelize.define("food", {
  name: { type: Sequelize.STRING }
});

async function create(food) {
  const foodCreated = await Food.create(food);

  return foodCreated;
}

async function findAllByName(name) {
  const food = await Food.findAll({
    where: {
      name
    }
  });

  return food;
}

async function findAllByCategory(categoryId) {
  const foods = await Food.findAll({
    where: {
      categoryId
    }
  });

  return foods;
}

async function findOneById(id) {
  const food = await Food.findById(id);
  return food;
}

async function updateOne(id, data) {
  await Food.update(data, {
    where: {
      id
    }
  });
}

sequelize.sync({
  alter: false,
  force: false
});

module.exports = {
  create,
  findOneById,
  findAllByName,
  findAllByCategory,
  updateOne,
  Model: Food
};
