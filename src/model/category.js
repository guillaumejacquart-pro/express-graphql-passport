import Sequelize from "sequelize";

const sequelize = require("./connection.js");

const Food = require("./food");

const Category = sequelize.define("category", {
  name: { type: Sequelize.STRING }
});

Food.Model.belongsTo(Category);
Category.hasMany(Food.Model);

async function create(category) {
  const categoryCreated = await Category.create(category);

  return categoryCreated;
}

async function getAll() {
  return Category.findAll();
}

sequelize.sync({
  alter: false
});

module.exports = {
  create,
  getAll
};
