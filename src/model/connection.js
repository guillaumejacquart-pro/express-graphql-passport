const Sequelize = require("sequelize");
const config = require("../config.js");

const sequelize = new Sequelize({
  host: config.get("db.host"),
  username: config.get("db.username"),
  password: config.get("db.password"),
  dialect: config.get("db.dialect"),
  database: config.get("db.name"),

  // SQLite only
  storage: config.get("db.storage"),

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});

module.exports = sequelize;
