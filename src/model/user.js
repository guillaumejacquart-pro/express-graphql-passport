import Sequelize from "sequelize";

const sequelize = require("./connection.js");

const User = sequelize.define("user", {
  uid: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  email: { type: Sequelize.STRING },
  week: { type: Sequelize.SMALLINT }
});

async function create(user) {
  const userCreated = await User.create(user);

  return userCreated;
}

async function findAll() {
  return await User.findAll();
}

async function findByUid(uid) {
  return await User.findByPrimary(uid);
}

sequelize.sync({
  force: false
});

module.exports = {
  create,
  findByUid,
  findAll
};
