import * as admin from "firebase-admin";
import config from "../config";

const serviceAccount = require(__dirname + "/credentials.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: config.get("firebaseUrl")
});

export default (req, res, next) => {
  admin
    .auth()
    .verifyIdToken(req.headers["x-token"])
    .then(function(decodedToken) {
      req.user = decodedToken;
      next();
    })
    .catch(function(error) {
      res.status(401).json(error);
    });
};
