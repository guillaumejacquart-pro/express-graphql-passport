import User from "../../model/user";

export default {
  getAll: async (_obj, _args, context) => {
    context.user;
    return await User.findAll();
  },
  me: async (_obj, _args, context) => {
    return await User.findByUid(context.user.uid);
  }
};
