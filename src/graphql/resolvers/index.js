import userResolvers from "./user";
import categoryResolvers from "./category";

// The resolvers
const resolvers = {
  Query: {
    categories: categoryResolvers.getAll,
    users: userResolvers.getAll,
    me: userResolvers.me
  },
  Mutation: {
    createCategory: categoryResolvers.create
  }
};

export default resolvers;
