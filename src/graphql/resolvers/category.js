import Category from "../../model/category";

export default {
  getAll: async () => await Category.getAll(),
  create: async (_, category) => await Category.create(category)
};