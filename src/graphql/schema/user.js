export const typeDef = `
    type User { 
        uid: String!, 
        email: String!,
        week: Int!
    }
`;
