import { makeExecutableSchema } from "graphql-tools";
import { merge } from "lodash";

import { typeDef as User } from "./user.js";
import { typeDef as Category } from "./category.js";

import resolvers from "../resolvers";

// The GraphQL schema in string form
const Query = `
    type Query { 
      categories(title: String): [Category],
      me: User,
      users: [User]
    }
    type Mutation {
      createCategory (
        title: String!,
        authorId: Int!
      ): Category
    }
  `;

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs: [Query, Category, User],
  resolvers: merge(resolvers)
});

export default schema;
